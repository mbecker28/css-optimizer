package de.marco.css.optimizer.framework;

import java.util.LinkedList;
import java.util.List;

import de.marco.CSSDocument;
import de.marco.css.optimizer.framework.rules.OptimizeRule;
import de.marco.property.CSSBlock;

public class CSSOptimizer {

	private final List<OptimizeRule> rules = new LinkedList<OptimizeRule>();

	public void addRule(final OptimizeRule rule) {
		rules.add(rule);
	}

	public CSSDocument optimizeDocument(final CSSDocument document) {
		final List<CSSBlock> blocks = document.getCssBlockList();
		for (final CSSBlock block : blocks) {
			for (final OptimizeRule rule : rules) {
				rule.handle(block);
			}
		}
		return document;
	}

}
