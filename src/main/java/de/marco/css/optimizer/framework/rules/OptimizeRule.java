package de.marco.css.optimizer.framework.rules;

import de.marco.property.CSSBlock;

public interface OptimizeRule {

	public void handle(CSSBlock block);

}
