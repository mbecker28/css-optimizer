package de.marco.css;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.marco.CSSDocument;
import de.marco.CSSParser;
import de.marco.css.optimizer.framework.CSSOptimizer;
import de.marco.css.rules.MarginRule;
import de.marco.css.rules.PaddingRule;

public class Main {

	public static void main(final String[] args) {
		try {
			final CSSParser parser = new CSSParser();
			final InputStream stream = ClassLoader.getSystemResourceAsStream("test.css");
			final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			final CSSDocument document = parser.parse(reader);

			final CSSOptimizer optimizer = new CSSOptimizer();
			optimizer.addRule(new PaddingRule());
			optimizer.addRule(new MarginRule());

			System.out.print(optimizer.optimizeDocument(document).getPlain(true));
		} catch (final Exception ex) {
			ex.printStackTrace();
		}
	}

}
