package de.marco.css.rules;

import java.util.List;

import de.marco.css.optimizer.framework.rules.OptimizeRule;
import de.marco.property.CSSBlock;
import de.marco.property.Property;
import de.marco.property.PropertyValue;
import de.marco.property.ValueEntry;

public abstract class AbstractDimensionRule implements OptimizeRule {

	private String[] properties;

	private CSSBlock block;

	private Property summary;

	private Property left;
	private Property right;
	private Property bottom;
	private Property top;

	public void handle(final CSSBlock block) {
		buildList();
		this.block = block;
		final List<Property> list = block.getProperties(properties);
		findElements(list);
		summery();
		compress();
	}

	private void buildList() {
		final String name = getSummeryName();
		properties = new String[] { name, name + "-left", name + "-right", name + "-bottom", name + "-top" };
	}

	public abstract String getSummeryName();

	private void summery() {
		if (bottom != null || left != null || right != null || top != null) {
			block.removeProperty(summary);
			summary = new Property();
			summary.setKey(getSummeryName());
			final PropertyValue propertyValue = summary.getSinglePropertyValue();
			if (top != null) {
				propertyValue.addEntry(top.getFirstEntry());
				block.removeProperty(top);
			} else {
				propertyValue.addEntry(getZeroEntry());
			}
			if (right != null) {
				propertyValue.addEntry(right.getFirstEntry());
				block.removeProperty(right);
			} else {
				propertyValue.addEntry(getZeroEntry());
			}
			if (bottom != null) {
				propertyValue.addEntry(bottom.getFirstEntry());
				block.removeProperty(bottom);
			} else {
				propertyValue.addEntry(getZeroEntry());
			}
			if (left != null) {
				propertyValue.addEntry(left.getFirstEntry());
				block.removeProperty(left);
			} else {
				propertyValue.addEntry(getZeroEntry());
			}
			block.addProperty(summary);
		}
	}

	private ValueEntry getZeroEntry() {
		final ValueEntry entry = new ValueEntry();
		entry.setValue("0");
		return entry;
	}

	private void compress() {
		if (summary != null) {
			final PropertyValue propertyValue = summary.getSinglePropertyValue();
			final List<ValueEntry> entries = propertyValue.getEntryList();
			final ValueEntry top = (entries.size() > 0 ? entries.get(0) : null);
			final ValueEntry right = (entries.size() > 1 ? entries.get(1) : null);
			final ValueEntry bottom = (entries.size() > 2 ? entries.get(2) : null);
			final ValueEntry left = (entries.size() > 3 ? entries.get(3) : null);
			switch (entries.size()) {
				case 2:
					if (top.equals(right)) {
						propertyValue.clear();
						propertyValue.addEntry(top);
					}
					break;
				case 3:
					if (top.equals(bottom)) {
						propertyValue.clear();
						propertyValue.addEntry(top);
						if (!top.equals(right)) {
							propertyValue.addEntry(right);
						}
					}
					break;
				case 4:
					if (top.equals(right) && right.equals(bottom) && bottom.equals(left)) {
						propertyValue.clear();
						propertyValue.addEntry(top);
					} else if (right.equals(left)) {
						propertyValue.clear();
						propertyValue.addEntry(top);
						propertyValue.addEntry(right);
						if (!top.equals(bottom)) {
							propertyValue.addEntry(bottom);
						}
					}
					break;
			}
		}
	}

	private void findElements(final List<Property> list) {
		for (final Property property : list) {
			if (property.getKey().equals(properties[0])) {
				summary = property;
			}
			if (property.getKey().equals(properties[1])) {
				left = property;
			}
			if (property.getKey().equals(properties[2])) {
				right = property;
			}
			if (property.getKey().equals(properties[3])) {
				bottom = property;
			}
			if (property.getKey().equals(properties[4])) {
				top = property;
			}
		}
	}

}
