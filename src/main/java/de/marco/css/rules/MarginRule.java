package de.marco.css.rules;

public class MarginRule extends AbstractDimensionRule {

	@Override
	public String getSummeryName() {
		return "margin";
	}

}
